(function() {
  var app = angular.module("githubViewer", []);

  //You need to inject all the services required to the controller, in this case you are using $scope and $http service
  var MainController = function($scope, $http) {

    var onUserComplete = function(response) {
      $scope.user = response.data;
    };

    var onError = function(reason) {
      $scope.error = "Could not fetch the user.";
    };

    $scope.search = function(username) {
      $http.get("https://api.github.com/users/" + username)
        .then(onUserComplete, onError);
    };

    $scope.username = "angular";
    $scope.message = "Github Viewer";
  };

  app.controller("MainController", ["$scope", "$http", MainController]);
}());

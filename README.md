angular-workshop
================

Learning AngularJS.

This is where I'll be putting all of my creations as I go through a series of tutorials and experiments.

I've found a variety of sources:
1. Angular in 60-ish minutes [link needed]
2. [Revillweb](http://code.tutsplus.com/tutorials/building-a-web-app-from-scratch-in-angularjs--net-32944)
3. [Shaping up with Angular.JS](http://campus.codeschool.com/courses/shaping-up-with-angular-js/intro)
4. [AngularJS: Get Started](https://app.pluralsight.com/library/courses/angularjs-get-started/table-of-contents)

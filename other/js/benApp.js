var bensApp = angular.module('benApp',['ngRoute']);

bensApp.controller('benCtrl',function($scope) {
  $scope.cats = [
    {'name': 'Kismet', 'breed': 'Domestic Shorthair'},
    {'name': 'Bandit', 'breed': 'Russian Blue'},
    {'name': 'Ezio', 'breed': 'Tabby'}
  ];
});
